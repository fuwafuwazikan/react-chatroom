import React from 'react';
import ChatColumn from './mods/ChatColumn';
import ChatMessages from './mods/ChatMessages';
import MessageFormat from './mods/MessageFormat';

export default class App extends React.Component {
  handleSendMessage = msg => {
    this.ws.send(msg);
  }

  handleReceiveMessage = msg => {
    msg = JSON.parse(msg.data);

    this.setState(state => ({
      'messages': state.messages.concat(
        new MessageFormat(
          msg.date,
          msg.sender,
          msg.content
        )
      )
    }));
  }

  componentWillMount() {
    this.setState({
      'messages': []
    });
  }

  componentDidMount() {
    this.ws.onmessage = this.handleReceiveMessage;
  }

  render() {
    return(
      <div id="app">
        <ChatMessages messages={this.state.messages} />
        <ChatColumn wsSend={this.handleSendMessage} />
      </div>
    )
  }

  constructor(props) {
    super(props);
    this.ws = new WebSocket(`ws://${window.location.hostname}:65080`);
  }
}