import WebSockets from 'ws';
import Clients from './mods/Clients';

const wsvr = new WebSockets.Server({
  'port': 65080
});

let clients = new Clients();

wsvr.on('connection', wsc => {
  wsc.on('message', msg => clients.broadcast(msg));
  wsc.on('close', () => clients.clientDisconnect(wsc));
  
  clients.clientConnect(wsc);
});